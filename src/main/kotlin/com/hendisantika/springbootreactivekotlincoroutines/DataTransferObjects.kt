package com.hendisantika.springbootreactivekotlincoroutines

import java.time.ZoneId
import java.time.ZonedDateTime

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-reactive-kotlin-coroutines
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 22/07/20
 * Time: 07.57
 */

class CounterEvent(
        val value: Long,
        val action: CounterAction,
        val at: ZonedDateTime = ZonedDateTime.now(ZoneId.of("UTC"))
)

enum class CounterAction { UP, DOWN }

class CounterState(
        val value: Long,
        val at: ZonedDateTime = ZonedDateTime.now(ZoneId.of("UTC"))
) {
    fun toEvent(action: CounterAction) = CounterEvent(value, action, at)
}