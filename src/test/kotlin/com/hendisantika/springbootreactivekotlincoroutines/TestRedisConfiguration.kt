package com.hendisantika.springbootreactivekotlincoroutines

import org.springframework.boot.test.context.TestConfiguration
import redis.embedded.RedisServer
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-reactive-kotlin-coroutines
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 22/07/20
 * Time: 08.09
 */
@TestConfiguration
internal class TestRedisConfiguration {
    private val redisServer = RedisServer(6379)

    @PostConstruct
    fun postConstruct() {
        redisServer.start()
    }

    @PreDestroy
    fun preDestroy() {
        redisServer.stop()
    }
}